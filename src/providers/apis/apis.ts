
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ApisProvider {

 
  constructor(public http: Http) {
    
  }

  
   signup_url = 'http://144.217.165.180:6013/sign_up';
   login_url = 'http://144.217.165.180:6013/login';
   nss_request_url = 'http://144.217.165.180:6013/request_nss_loan';
   retrieve_details_url = 'http://144.217.165.180:6013/retrieve_sub';
   check_nss_loan_url = 'http://144.217.165.180:6013/nss_loan_check';
   



registration(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.signup_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


pocket_login(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.login_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

retrieve_details(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_details_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


nss_request(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.nss_request_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


check_nss_loan(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.check_nss_loan_url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

 

}
