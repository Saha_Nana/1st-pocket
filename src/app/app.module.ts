import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http'
// import { HttpClient } from '@angular/common/http';

// import { AboutPage } from '../pages/about/about';
// import { ContactPage } from '../pages/contact/contact';
// import { HomePage } from '../pages/home/home';
// import { TabsPage } from '../pages/tabs/tabs';
// import { WelcomePage } from '../pages/welcome/welcome';
// import { ApplyLoanPage } from '../pages/apply-loan/apply-loan';
// import { AccountPage } from '../pages/account/account';
// import { SalaryAdvanceFormPage } from '../pages/salary-advance-form/salary-advance-form';
// import { NssLoanFormPage } from '../pages/nss-loan-form/nss-loan-form';
// import { SalaryLoanFormPage } from '../pages/salary-loan-form/salary-loan-form';
// import { MmFormPage } from '../pages/mm-form/mm-form';
// import { BankFormPage } from '../pages/bank-form/bank-form';
// import { MerchantFormPage } from '../pages/merchant-form/merchant-form';
// import { AccBalancePage } from '../pages/acc-balance/acc-balance';
// import { LoginPage } from '../pages/login/login'
// import { SignupPage } from '../pages/signup/signup'
// import { ProfilePage } from '../pages//profile/profile';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { ApisProvider } from '../providers/apis/apis';

// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';


@NgModule({
  declarations: [
    MyApp,
    // AboutPage,
    // ContactPage,
    // HomePage,
    // TabsPage,
    // WelcomePage,
    // ApplyLoanPage,
    // AccountPage,
    // SalaryAdvanceFormPage,
    // NssLoanFormPage,
    // SalaryLoanFormPage,
    // MmFormPage,
    // BankFormPage,
    // MerchantFormPage,
    // AccBalancePage,
    // LoginPage,
    // SignupPage,
    // ProfilePage
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    // AboutPage,
    // ContactPage,
    // HomePage,
    // TabsPage,
    // WelcomePage,
    // ApplyLoanPage,
    // AccountPage,
    // SalaryAdvanceFormPage,
    // NssLoanFormPage,
    // SalaryLoanFormPage,
    // MmFormPage,
    // BankFormPage,
    // MerchantFormPage,
    // AccBalancePage,
    // LoginPage,
    // SignupPage,
    // ProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    Transfer,
    Camera,
    FilePath,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApisProvider
  ]
})
export class AppModule {}
