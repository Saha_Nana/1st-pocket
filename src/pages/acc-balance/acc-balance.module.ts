import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccBalancePage } from './acc-balance';

@NgModule({
  declarations: [
    AccBalancePage,
  ],
  imports: [
    IonicPageModule.forChild(AccBalancePage),
  ],
})
export class AccBalancePageModule {}
