import { Component } from '@angular/core';
import { IonicPage,NavController, NavParams } from 'ionic-angular';
import { ToastController, LoadingController, AlertController, ActionSheetController, Platform, Loading } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-merchant-form',
  templateUrl: 'merchant-form.html',
})
export class MerchantFormPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MerchantFormPage');
  }

  transfer(){
    let alert = this.alertCtrl.create({

      subTitle: 'Not ready!',
      buttons: ['Ok']
    });
    alert.present();
  }

}
