import { Component } from '@angular/core';
// import { ApplyLoanPage } from '../apply-loan/apply-loan';
// import { AccountPage } from '../account/account';
// import { ProfilePage } from '../profile/profile';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApisProvider } from '../../providers/apis/apis';
import { LoadingController, AlertController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  from_login: any = []
  body: any;
  jsonBody: any;
  username: any;
  status: any;
  retrieve: any;

  constructor(private inAppBrowser: InAppBrowser, public navCtrl: NavController, public alertCtrl: AlertController, public api: ApisProvider, public modalCtrl: ModalController, public navParams: NavParams, public loadingCtrl: LoadingController) {

    this.from_login = this.navParams.data
    this.body = this.from_login
    this.jsonBody = JSON.parse(this.body);
    this.username = this.jsonBody[0].username
    this.status = this.jsonBody[0].status

    console.log("USERNAME HERE IS " + this.username)
    console.log("Registration Status HERE IS " + this.status)

  }

  refresh() {



    this.jsonBody = {
      "username": this.username
    }

    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();

    this.api.retrieve_details(this.jsonBody).then((result) => {

      var body1 = result["_body"];
      body1 = JSON.parse(body1);

      this.retrieve = body1
      this.retrieve = JSON.stringify(body1)

    
      this.status = body1[0].status

      console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve);
      console.log('REFRESH STATUS IS ' + this.status);
      
    });

    loader.dismiss();
  }


  alert() {
    let alert = this.alertCtrl.create({

      subTitle: 'Not available',
      buttons: ['Ok']
    });
    alert.present();
  }

  hire() {

    const options: InAppBrowserOptions = {
      zoom: 'no'
    }


    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();

    const browser = this.inAppBrowser.create("https://www.pickoncredit.com/", '_self', options);
    browser.close;
    loader.dismiss();
  }

  profile() {

    let alert = this.alertCtrl.create({
      title: "",
      subTitle: "Feature not available now",
      buttons: ['OK']
    });
    alert.present();
    // this.navCtrl.push("ProfilePage")

  }


  loan() {

    if (this.status == false) {
      let alert = this.alertCtrl.create({
        title: "",
        subTitle: "Kindly be patient as we verify your registration before you can apply for a loan. You would be contacted by our representative soon. Thank you",
        buttons: ['OK']
      });
      alert.present();
    }

    if (this.status == true) {
      this.navCtrl.push("ApplyLoanPage", { value: this.from_login })
    }
  }




  account() {
    this.navCtrl.push("AccountPage")
  }



}
