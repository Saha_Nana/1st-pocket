import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MmFormPage } from './mm-form';

@NgModule({
  declarations: [
    MmFormPage,
  ],
  imports: [
    IonicPageModule.forChild(MmFormPage),
  ],
})
export class MmFormPageModule {}
