import { Component } from '@angular/core';



import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApisProvider } from '../../providers/apis/apis';
import {  LoadingController, AlertController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = "HomePage";
  tab2Root = "AboutPage";
  

  from_login: any = []
  body: any;
  jsonBody: any;
  username: any;

  // constructor() {

  // }

  constructor( public api: ApisProvider, public alertCtrl: AlertController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
    
    this.from_login = this.navParams.get('value')
   
    console.log('LOGIN DETAILS IN TAB CONSTRUCTOR IS' + this.from_login);
   


    this.body = Array.of(this.from_login)
    this.jsonBody = JSON.parse(this.body);
  
    this.username = this.jsonBody[0].username
   
    console.log("USERNAME HERE IS " + this.username)

          this.tab1Root = "HomePage";
        //  this.tab2Root = "AboutPage";
        


  }
}
