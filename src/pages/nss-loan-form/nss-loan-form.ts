import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastController, LoadingController, AlertController, ActionSheetController, Platform, Loading } from 'ionic-angular';

import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { ApisProvider } from '../../providers/apis/apis';



declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-nss-loan-form',
  templateUrl: 'nss-loan-form.html',
})
export class NssLoanFormPage {
  lastImage: string = null;
  loading: Loading;

  public nssForm: any;
  from_hosp: any;
  from_login: any;
  nssVal: any;
  jsonBody: any;
  body: any;
  other_names: any;
  params: any;
  surname: any;
  email: any;
  mobile_number: any;
  dob: any;
  messageList: any;
  api_code: any;


  constructor(public api: ApisProvider, public _form: FormBuilder, public platform: Platform, public toastCtrl: ToastController, public http: Http, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public app: App, private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public navCtrl: NavController, public navParams: NavParams) {

    this.from_login = this.navParams.get('value')
    this.body = this.from_login
    this.jsonBody = JSON.parse(this.body);

    this.surname = this.jsonBody[0].surname
    this.other_names = this.jsonBody[0].other_names
    this.mobile_number = this.jsonBody[0].mobile_number
    this.email = this.jsonBody[0].email
    this.dob = this.jsonBody[0].dob
    console.log("FROM LOGIN IS " + this.from_login)
    console.log("other_names HERE IS " + this.other_names)




    this.nssForm = this._form.group({

      "institution_name": ["", Validators.compose([Validators.required])],
      "nss_number": ["", Validators.compose([Validators.required])],
      "loan_purpose": ["", Validators.compose([Validators.required])],
      "loan_amount": ["", Validators.compose([Validators.required])],
      "account_number": ["", Validators.compose([Validators.required])],


    })
  }



  salary_loan() {

    this.nssVal = JSON.stringify(this.nssForm.value);

    this.jsonBody = JSON.parse(this.nssVal);

    console.log("THIS IS THE Appoint raw values VALUES" + this.nssVal)
    console.log("THIS IS THE Appoint VALUES " + this.jsonBody)
    console.log("THIS IS THE PROPOSED TIME" + this.jsonBody.institution_name)


    this.params = {
      "surname": this.surname,
      "other_names": this.other_names,
      "mobile_number": this.mobile_number,
      "email": this.email,
      "dob": this.dob,
      "institution_name": this.jsonBody.institution_name,
      "nss_number": this.jsonBody.nss_number,
      "loan_purpose": this.jsonBody.loan_purpose,
      "loan_amount": this.jsonBody.loan_amount,
      "account_number": this.jsonBody.account_number,
    }



    let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });

    loader.present();


    this.api.nss_request(this.params).then((result) => {

      console.log("THIS IS THE RESULT" + result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)


      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

      if (this.api_code == "200") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: "Your request for Nss Loan has been submitted successfully. We would get in touch with you soon",
          buttons: ['OK']
        });
        alert.present();
      }
      this.navCtrl.setRoot("TabsPage", { value: this.from_login });



      if (this.api_code != "200") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }


    }, (err) => {
      loader.dismiss();
      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();

      console.log(err);
    });
  }



  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }




  public uploadImage() {
    // Destination URL

    var url = "http://144.217.165.180:6013/save_image";

    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);
    console.log("LETS SEE THE TARGET PATH" + targetPath)

    // File name only
    var filename = this.lastImage;
    console.log("LETS SEE THE FILE ABOUT TO BE UPLOADED" + filename)

    //options
    var options = {
      fileKey: "fileName",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: { 'fileName': filename }

    };

    const fileTransfer: TransferObject = this.transfer.create();

    let loader = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    
    loader.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {

      console.log("LETS SEE THE DATA COMING" + data)
      console.log("LETS SEE THE DATA COMING IN JSON" + JSON.stringify(data));
      loader.dismiss();
      this.presentToast('Image succesful uploaded.');
    }, err => {

      loader.dismiss();
      this.presentToast('Error while uploading file.');
    });
  }

}
