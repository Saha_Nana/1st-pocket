import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NssLoanFormPage } from './nss-loan-form';

@NgModule({
  declarations: [
    NssLoanFormPage,
  ],
  imports: [
    IonicPageModule.forChild(NssLoanFormPage),
  ],
})
export class NssLoanFormPageModule {}
