import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalaryAdvanceFormPage } from './salary-advance-form';

@NgModule({
  declarations: [
    SalaryAdvanceFormPage,
  ],
  imports: [
    IonicPageModule.forChild(SalaryAdvanceFormPage),
  ],
})
export class SalaryAdvanceFormPageModule {}
