import { Component } from '@angular/core';
import {  IonicPage,NavController, NavParams } from 'ionic-angular';
// import { TabsPage } from '../tabs/tabs'
// import {  LoginPage } from '../login/login'
// import { SignupPage } from '../signup/signup'

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }



  go(){
    this.navCtrl.push("TabsPage")
  }

  login(){
    this.navCtrl.push("LoginPage")
  }

  signup(){
    this.navCtrl.push("SignupPage")

  }

}
