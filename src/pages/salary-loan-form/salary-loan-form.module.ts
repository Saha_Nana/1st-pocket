import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalaryLoanFormPage } from './salary-loan-form';

@NgModule({
  declarations: [
    SalaryLoanFormPage,
  ],
  imports: [
    IonicPageModule.forChild(SalaryLoanFormPage),
  ],
})
export class SalaryLoanFormPageModule {}
