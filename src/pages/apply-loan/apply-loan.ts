import { Component } from '@angular/core';
import { IonicPage,NavController,ToastController, LoadingController, AlertController , NavParams } from 'ionic-angular';
import { ApisProvider } from '../../providers/apis/apis';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-apply-loan',
  templateUrl: 'apply-loan.html',
})
export class ApplyLoanPage {
  from_login: any = []
  from_hosp: any;
  nssVal: any;
  jsonBody: any;
  body: any;
  other_names: any;
  params: any = [];
  surname: any;
  email: any;
  mobile_number: any;
  mobile_number1: any;
  dob: any;
  messageList: any;
  api_code: any;
  body1: any;
  
  retrieve: string;

  constructor(public toastCtrl: ToastController,public alertCtrl: AlertController,public loadingCtrl: LoadingController,public http: Http,public httpClient: HttpClient,public api: ApisProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.from_login = this.navParams.get('value')

    this.body = this.from_login
    this.jsonBody = JSON.parse(this.body);
   
    this.mobile_number = this.jsonBody[0].mobile_number
    
    
    console.log("Mobile Number IS " + this.mobile_number)
  }

 

 



  request_advance(){ 
    this.navCtrl.push("SalaryAdvanceFormPage")

  }

  nss_loan(){

    this.params = {

      "mobile_number": this.mobile_number
  
    }

    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();

    this.api.check_nss_loan(this.params).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;


      loader.dismiss();

      if (this.api_code != "206") {
        let loader = this.loadingCtrl.create({
          content: "Login processing..."
        });

        loader.present();

        setTimeout(() => {
          this.navCtrl.push("NssLoanFormPage",{value: this.from_login});

        }, 3000);

        setTimeout(() => {
          loader.dismiss();
        }, 3000);

      }

      if (this.api_code == "206") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: "Sorry. You are not elligible to apply for the next Nss Loan",
          buttons: ['OK']
        });
        alert.present();
      }
    },
    (err) => {

      let alert = this.alertCtrl.create({
        title: "",
        subTitle: "No internet connection",
        buttons: ['OK']
      });
      alert.present();

      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();
      loader.dismiss();
      console.log(err);
    });


  }

  salary_loan(){
    this.navCtrl.push("SalaryLoanFormPage")

  }

}
