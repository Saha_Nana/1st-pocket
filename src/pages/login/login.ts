
import { Component } from '@angular/core';
import { IonicPage,NavController, NavParams } from 'ionic-angular';
// import { HomePage } from '../home/home';
// import { SignupPage } from '../signup/signup';
// import { TabsPage } from '../tabs/tabs';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { ApisProvider } from '../../providers/apis/apis';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  splash = true;

  public loginForm: any;
  submitAttempt: boolean = false;
  loginVal: any;
  jsonBody: any;
  jsonBody1: any;

  messageList: any;
  api_code: any;
  Phonenumber: string;
  PIN: string;
  retrieve: string;
  retrieve1: string;
  retrieve_pers: string;
  retrieve_doc: string;
  retrieve_doc3: string;

  searchbar: any;
  from_login: any = [];

  location: any;
  displayData: any;
  check: any;
  from_menu: any = [];
  body: any;
  body1: any;
  person_type: any;
  person_type1: any;
  doctor_id: any;
  id: any;
  doc_id: any;
  doctor_id2: any;
  doc_params: any = [];
  doc_params2: any = [];

  // params: any = [];

  requester_id: any;
  data1: any = [];

  constructor(public toastCtrl: ToastController, public api: ApisProvider, public _form: FormBuilder, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public http: Http,public httpClient: HttpClient) {

    this.loginForm = this._form.group({

      "password": ["", Validators.compose([Validators.required])],
      "username": ["", Validators.compose([Validators.required])]



    })

  }

 


  // login(){
  //   this.navCtrl.push(TabsPage)
  // }

  signup(){
    this.navCtrl.setRoot("SignupPage")
  }



  login() {

    this.loginVal = JSON.stringify(this.loginForm.value);
    console.log("LETS SEE THE LOGIN VAL " + this.loginVal)

    this.jsonBody = JSON.parse(this.loginVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE SIGNUP VALUES STRINGIFY" + JSON.stringify(this.jsonBody))



    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();

    this.api.retrieve_details(this.jsonBody).then((result) => {

      var body1 = result["_body"];
      body1 = JSON.parse(body1);

      this.retrieve = body1
      this.retrieve = JSON.stringify(body1)

      console.log('LETS SEE THE  BODY ' + body1);
      console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve);
    });


    this.api.pocket_login(this.jsonBody).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;


      loader.dismiss();
      if (this.api_code == "200") {
        let loader = this.loadingCtrl.create({
          content: "Login processing..."
        });

        loader.present();

        setTimeout(() => {
          this.navCtrl.setRoot("TabsPage",

            { value: this.retrieve });

        }, 3000);

        setTimeout(() => {
          loader.dismiss();
        }, 3000);

      }
      if (this.api_code != "200") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }
    },

      (err) => {

        let alert = this.alertCtrl.create({
          title: "",
          subTitle: "No internet connection",
          buttons: ['OK']
        });
        alert.present();

        this.toastCtrl.create({
          message: "Please check your internet connection",
          duration: 5000
        }).present();
        loader.dismiss();
        console.log(err);
      });
  }

}
