import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
// import { MmFormPage } from '../mm-form/mm-form';
// import { BankFormPage } from '../bank-form/bank-form';
// import { MerchantFormPage } from '../merchant-form/merchant-form';
// import { AccBalancePage } from '../acc-balance/acc-balance';
import { IonicPage,ToastController, LoadingController, AlertController, ActionSheetController, Platform, Loading } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }

  mobile_money(){
    this.navCtrl.push("MmFormPage")

  }

  merchant(){
    this.navCtrl.push("MerchantFormPage")
  }

  bank(){
    this.navCtrl.push("BankFormPage")

  }

  balance(){


      let alert = this.alertCtrl.create({
  
        subTitle: 'Available Balance: GHC 0.00',
        buttons: ['Ok']
      });
      alert.present();
    }
   


}
