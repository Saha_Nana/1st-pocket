import { Component } from '@angular/core';
import { IonicPage,NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { Http } from '@angular/http';
// import { LoginPage } from '../login/login';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';

import 'rxjs/add/operator/map';



@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',

})

export class ProfilePage {


  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

  }




  ionViewDidEnter() {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }




  profile() {
    let alert = this.alertCtrl.create({

      subTitle: 'Not available',
      buttons: ['Ok']
    });
    alert.present();

  }


}
